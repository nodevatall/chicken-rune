﻿using UnityEngine;
using System.Collections;

public class LRAnimControllerScript : MonoBehaviour {

    private bool _flipped;
    private Vector2 _last;

    // Use this for initialization
    void Start () {
        this._flipped = false;
        this._last = GetComponent<Rigidbody2D>().velocity;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        Vector2 cur = GetComponent<Rigidbody2D>().velocity;
        float move = 0f;
        if((cur.x > 0.5 || cur.x < -0.5) || (cur.y > 0.5 || cur.y < -0.5) || (this._last.x != cur.x || this._last.y != cur.y))
        {
            move = cur.x;
            if (move > 0 && !this._flipped) this.Flip();
            if (move < 0 && this._flipped) this.Flip();
            if (move == 0) move = cur.y;
        }
        GetComponent<Animator>().SetFloat("Speed",Mathf.Abs(move));

        this._last = cur;
    }

    // Flip
    void Flip()
    {
        Vector3 scale = GetComponent<Transform>().localScale;
        scale.x *= -1;
        GetComponent<Transform>().localScale = scale;
        if (this._flipped) this._flipped = false;
        else this._flipped = true;
    }
}
