﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Main : MonoBehaviour
{

    //Variables
    public GameObject message;
    private GameObject poulet;
	public GameObject poulet2;
    private GameObject fxPenta;
    private GameObject fxDicso1;
    private GameObject fxDicso2;
    public bool pleinEcran = false;
    private GameObject ecranTitre;
    private GameObject ecranIntro;

    // Use this for initialization
    void Start()
    {
        scriptGlobal.listeCultistes = new List<Cultiste>();
        message = GameObject.Find("Message").gameObject;
        poulet = GameObject.Find("Joueur").gameObject;
        poulet2 = GameObject.Find("Joueur2").gameObject;
        poulet.SetActive(false);
        poulet2.SetActive(false);
        fxPenta = GameObject.Find("Fond").transform.FindChild("effetPenta").gameObject;
        fxPenta.SetActive(false);
        fxDicso1 = GameObject.Find("Fond").transform.FindChild("effetDisco1").gameObject;
        fxDicso1.SetActive(false);
        fxDicso2 = GameObject.Find("Fond").transform.FindChild("effetDisco2").gameObject;
        fxDicso2.SetActive(false);
        ecranTitre = GameObject.Find("Canvas").transform.FindChild("EcranTitre").gameObject;
        ecranIntro = GameObject.Find("Canvas").transform.FindChild("EcranIntro").gameObject;
		GameObject.Find("Canvas").transform.FindChild("Cine1").gameObject.SetActive(false);
		GameObject.Find("Canvas").transform.FindChild("Cine2").gameObject.SetActive(false);
        MaJInterfaceVie();
        //Musique de l'ecran titre
        scriptMusiques.Instance.MakeTheme("ThemePrincipal");
    }

	//Update
	void Update()
	{
		if(scriptGlobal.etatJeu == "init")
		{
			if(Input.GetAxis("Submit") > 0 && scriptGlobal.modeMenuActif == true)
			{
				ClicEcranTitre();
			}

		}
		//mode 2 joueurs
		else if(scriptGlobal.etatJeu == "play")
		{
			if(Input.GetAxis("Submit") > 0)
			{
				poulet2.SetActive(true);
				scriptGlobal.joueur2Mode = true;
				GameObject.Find("Canvas").transform.FindChild("Label2P").gameObject.SetActive(false);
			}

		}
	}
    public void activerMode2Joueur()
    {
        scriptGlobal.joueur2Mode = true;
        ClicEcranTitre();
    }

    public void mettrePleinEcran()
    {
        if (!pleinEcran)
        {
            Screen.fullScreen = true;
            pleinEcran = true;
        }
        else
        {
            Screen.fullScreen = false;
            pleinEcran = false;
        }
    }
    //Interface
    //Barre de vie
    public void MaJInterfaceVie()
    {
        GameObject intVie = GameObject.Find("InterfaceVie").transform.gameObject;
        int nbrChild = intVie.transform.childCount;
        for (int c = 0; c < nbrChild; c++)
        {
            if (c < scriptGlobal.viePouletMax)
            {
                intVie.transform.GetChild(c).gameObject.SetActive(true);
                //On checke si le coeur est actif ou perdu
                if (c > scriptGlobal.viePoulet - 1)
                {
                    intVie.transform.GetChild(c).gameObject.transform.GetComponent<Image>().color = new Color(intVie.transform.GetChild(c).gameObject.transform.GetComponent<Image>().color.r, intVie.transform.GetChild(c).gameObject.transform.GetComponent<Image>().color.g, intVie.transform.GetChild(c).gameObject.transform.GetComponent<Image>().color.b, 0.3f);
                }
                else {
                    intVie.transform.GetChild(c).gameObject.transform.GetComponent<Image>().color = new Color(intVie.transform.GetChild(c).gameObject.transform.GetComponent<Image>().color.r, intVie.transform.GetChild(c).gameObject.transform.GetComponent<Image>().color.g, intVie.transform.GetChild(c).gameObject.transform.GetComponent<Image>().color.b, 1f);
                }
            }
            else {
                intVie.transform.GetChild(c).gameObject.SetActive(false);
            }
        }
    }

    //Modification vie
    public void ModifierVie(int modifVie)
    {
        scriptGlobal.viePoulet += modifVie;
        //Fx
        scriptFX.Instance.MakeFx("FxHit1");
        //Vie max
        if (scriptGlobal.viePoulet > scriptGlobal.viePouletMax)
        {
            scriptGlobal.viePoulet = scriptGlobal.viePouletMax;
            //MaJ de l'interface de vie
            MaJInterfaceVie();
        }
		else if (scriptGlobal.viePoulet == 0 && scriptGlobal.etatJeu == "play")
        {
			//Mort du poulet
			scriptGlobal.etatJeu = "dead";
			StopCoroutine(GestionDesVagues());
			//MaJ de l'interface de vie
            MaJInterfaceVie();
            message.GetComponent<Text>().text = "CHICKEN IS DEAD !!!";
            //poulet.SetActive (false);
            poulet.GetComponent<Animator>().SetTrigger("dead");
            if (scriptGlobal.joueur2Mode)
            {
                poulet2.GetComponent<Animator>().SetTrigger("dead");
            }
            Invoke("mortDuPoulet", 2.5f);
        }
        else {
            //MaJ de l'interface de vie
            MaJInterfaceVie();
        }
    }
    private void mortDuPoulet()
    {
        poulet.SetActive(false);
        poulet2.SetActive(false);
        ResetJeu();
    }

    //gestion de l'apparition d'un bonus
    public void GestionApparitionBonus(Vector3 positionBonus, bool isBoss)
    {
        GameObject bonus;
        //Si c'est un boss, on fait obligatoirement apparaitre le bonus de level up
        if (isBoss)
        {
            bonus = Instantiate(Resources.Load("Bonus/BonusLevelUp")) as GameObject;
            bonus.transform.position = positionBonus;
            bonus.transform.parent = GameObject.Find("LesBonus").transform;
        }
        else {
            //Fx mort
            scriptFX.Instance.MakeFx("FxCultistMort1");
            //On checke si un bonus apparait
            int proba = (int)Random.Range(0f, 100.5f); //Entre 0 et 100
            if (proba <= scriptGlobal.pourcBonus)
            {
                //Apparition d'un bonus
                //Choix du bonus
                int typeBonus = (int)Random.Range(1f, 3.5f); //Entre 1 et 3
                switch (typeBonus)
                {
                    //Bonus boost vitesse
                    case 1:
                        bonus = Instantiate(Resources.Load("Bonus/BonusBoostVitesse")) as GameObject;
                        break;
                    //Bonus coeur
                    case 2:
                        bonus = Instantiate(Resources.Load("Bonus/BonusCoeur")) as GameObject;
                        break;
                    //Bonus slow ennemi
                    case 3:
                        bonus = Instantiate(Resources.Load("Bonus/BonusSlowEnnemi")) as GameObject;
                        break;
                    default:
                        bonus = Instantiate(Resources.Load("Bonus/BonusBoostVitesse")) as GameObject;
                        break;
                }
                bonus.transform.position = positionBonus;
                bonus.transform.parent = GameObject.Find("LesBonus").transform;
            }
        }
    }

    //Level up du poulet
    public void LevelUp()
    {
        //Fx
        scriptFX.Instance.MakeFx("FxPowerUp");
        //Gain d'un coeur de vie
        scriptGlobal.viePouletMax += 1;
        scriptGlobal.viePoulet = scriptGlobal.viePouletMax;
        //Gain de vitesse
        scriptGlobal.vitessePoulet += 0.5f;
        //MaJ de l'interface de vie
        MaJInterfaceVie();
    }

    //Boost vitesse
    public void BoostVitesse()
    {
        //Fx
        scriptFX.Instance.MakeFx("FxPowerUp");
        scriptGlobal.vitessePoulet += scriptGlobal.gainBoostVitesse;
        //Temps d'attente
        StartCoroutine(DureeBoostVitesse());
    }
    IEnumerator DureeBoostVitesse()
    {
        yield return new WaitForSeconds(scriptGlobal.dureeBoostVitesse);
        scriptGlobal.vitessePoulet -= scriptGlobal.gainBoostVitesse;
    }

    //Slow ennemi
    public void SlowEnnemi()
    {
        //Fx
        scriptFX.Instance.MakeFx("FxPowerUp");
        scriptGlobal.modifSlowEnnemi = -2;
        //Temps d'attente
        StartCoroutine(DureeSlowEnnemi());
    }
    IEnumerator DureeSlowEnnemi()
    {
        yield return new WaitForSeconds(scriptGlobal.dureeSlowEnnemi);
        scriptGlobal.modifSlowEnnemi = 0;
    }

    //Gain coeur
    public void GainCoeur()
    {
        //Fx
        scriptFX.Instance.MakeFx("FxPowerUp");
        scriptGlobal.viePoulet += 1;
        if (scriptGlobal.viePoulet > scriptGlobal.viePouletMax) scriptGlobal.viePoulet = scriptGlobal.viePouletMax;
        //MaJ de l'interface de vie
        MaJInterfaceVie();
    }

    //Reset du jeu
    public void ResetJeu()
    {
        //Reset List Cultiste dans pentagramme :
        GameObject.Find("pentagramme").GetComponent<pentagrammeScript>().ennemiPresentDansLePentagramme.Clear();
        GameObject.Find("pentagramme").GetComponent<pentagrammeScript>().ennemiASupprimer.Clear();
        //On efface tous les cultistes
        int nbrChild = GameObject.Find("LesCultistes").transform.childCount;
        for (int c = nbrChild - 1; c >= 0; c--)
        {
            Destroy(GameObject.Find("LesCultistes").transform.GetChild(c).gameObject);
        }
        //Reset vague
        scriptGlobal.bossTime = false;
        scriptGlobal.nbrVagues = 1;
		scriptGlobal.tempsEntreVagues = scriptGlobal.tempsVague1;
        //Reset vie poulet
        scriptGlobal.viePouletMax = 3;
        scriptGlobal.viePoulet = scriptGlobal.viePouletMax;
        //Reset vitesse poulet
        scriptGlobal.vitessePoulet = 3;
        //Reset modif vitesse ennemi bonus
        scriptGlobal.modifSlowEnnemi = 0;
        //Stage actuel
        scriptGlobal.stageActu = 1;
        //Stop effets
		StopCoroutine(PentaBrille(0));
        scriptFX.Instance.MakeFx("FxMort1");
        //Reset Rune :
        GameObject.Find("World").GetComponent<RunesManager>().sortFini();
        //On relance le jeu
        StartCoroutine(RelancerJeu());
    }
    IEnumerator RelancerJeu()
    {
        yield return new WaitForSeconds(2f);
		//Reset des runes
		GameObject.Find("World").GetComponent<RunesManager>().resetRunes();
		TerminerFxDisco();
        //Reset du fond
        GameObject.Find("Fond").GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Fonds/fond" + scriptGlobal.stageActu) as Sprite;
        //MaJ des interfaces
        MaJInterfaceVie();
        MaJNumeroVague();
		//Musique
		scriptMusiques.Instance.StopTheme();
		//Fx
        scriptFX.Instance.MakeFx("FxTransitionMort");
        //Compte a rebourd
        message.GetComponent<Text>().text = "3";
        yield return new WaitForSeconds(0.5f);
        message.GetComponent<Text>().text = "2";
        yield return new WaitForSeconds(0.5f);
        message.GetComponent<Text>().text = "1";
        yield return new WaitForSeconds(0.5f);
		message.GetComponent<Text>().text = "Don't chicken out !";
        yield return new WaitForSeconds(1);
        message.GetComponent<Text>().text = "";
		//Invincibilite off
		scriptGlobal.pouletInvincible = false;
        //On reaffiche le poulet au centre
        poulet.transform.position = new Vector2(0, 0);
        poulet.SetActive(true);
        if (scriptGlobal.joueur2Mode)
        {
            poulet2.transform.position = new Vector2(0, -1.5f);
            poulet2.SetActive(true);
        }
        scriptGlobal.etatJeu = "play";
        //Musique
        scriptMusiques.Instance.MakeTheme("ThemeStage" + scriptGlobal.stageActu);
        StartCoroutine(GestionDesVagues());
    }

    //Numero de la vague
    public void MaJNumeroVague()
    {
        GameObject.Find("InterfaceVague").transform.GetComponent<Text>().text = "Wave " + scriptGlobal.nbrVagues;
    }

    public IEnumerator NextLevel()
    {
        scriptGlobal.bossTime = false;
        Debug.Log("Next Level: " + scriptGlobal.bossTime);
        //Fx mort du boss
        scriptMusiques.Instance.StopTheme();
        scriptFX.Instance.MakeFx("FxBossMort1");
        yield return new WaitForSeconds(2);
        //Musique de changement de niveau
        scriptFX.Instance.MakeFx("FxTransitionStage");
        yield return new WaitForSeconds(3.5f);
		if(scriptGlobal.nbrVagues >= (5*scriptGlobal.pallierVagues))
        {
            //Si tous les ennemis sont morts, on entraine la fin du jeu
            if (GameObject.Find("LesCultistes").transform.childCount == 0)
            {
                scriptGlobal.etatJeu = "win";
                message.GetComponent<Text>().text = "CONGRATULATION ! CHICKEN WIN THE RITUAL !";
				//Fx victoire
				scriptFX.Instance.MakeFx("FxLevelComplete");
				yield return new WaitForSeconds(5);
				ResetJeu();
            }
        }
        else {
			//Invincibilite off
			scriptGlobal.pouletInvincible = false;
			message.GetComponent<Text>().text = "NEXT STAGE !";
			scriptGlobal.stageActu++;
			//Changement d'intervalle des vagues
			if(scriptGlobal.stageActu >= 4) {
				scriptGlobal.tempsEntreVagues = scriptGlobal.tempsVague2;
			}
            if (scriptGlobal.stageActu == 2) FxDisco();
            else if (scriptGlobal.stageActu == 3) TerminerFxDisco();
            GameObject.Find("Fond").GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Fonds/fond" + scriptGlobal.stageActu) as Sprite;
            scriptGlobal.bossTime = false;
            yield return new WaitForSeconds(1);
            message.GetComponent<Text>().text = "";
            scriptGlobal.bossTime = false;
            Debug.Log("Start coroutine: " + scriptGlobal.bossTime);
            //Musique du stage
            scriptMusiques.Instance.MakeTheme("ThemeStage" + scriptGlobal.stageActu);
            StartCoroutine(GestionDesVagues());
        }
    }
    public void nextLevelBefore()
    {
        StartCoroutine(NextLevel());
    }

    //Gestion enchainement des vagues
    public IEnumerator GestionDesVagues()
    {
        //Debug.Log ("GDV: nbr = "+scriptGlobal.nbrVagues+", bt:"+scriptGlobal.bossTime);
		if (!scriptGlobal.bossTime && scriptGlobal.nbrVagues <= (5*scriptGlobal.pallierVagues))
        {
            //Debug.Log ("GDV: exec");
            GestionVague();
            //Debug.Log ("GDV: ok");
            yield return new WaitForSeconds(scriptGlobal.tempsEntreVagues);
            if (scriptGlobal.etatJeu == "play")
            {
                //Debug.Log ("GDV start co rout bossTime: "+scriptGlobal.bossTime);
                if (!scriptGlobal.bossTime) StartCoroutine(GestionDesVagues());
                /*
				//Tant qu'un boss est present, on ne lance la pas vague suivante
				bool bossPresent = false;
				int l = GameObject.Find ("LesCultistes").transform.childCount;
				for (int c = 0; c < l; c++) {
					if (GameObject.Find ("LesCultistes").transform.GetChild (c).gameObject.name.Substring (0, 8) == "CultBoss")
						bossPresent = true;
				}
                print("bossPresent : " + bossPresent);
				if (bossPresent == false) {

				}
					//On change le fond si on est après une vague modulo de 5
					if (scriptGlobal.nbrVagues % 5 == 1) {
						//On checke si c'est la fin du jeu
						if (scriptGlobal.nbrVagues >= 26) {
							//Si tous les ennemis sont morts, on entraine la fin du jeu
							if (GameObject.Find ("LesCultistes").transform.childCount == 0) {
								scriptGlobal.etatJeu = "win";
								yield return new WaitForSeconds (1);
								scriptGlobal.nbrVagues++;
								StartCoroutine (GestionDesVagues ());
							}
						} else {
							scriptGlobal.nbrVagues++;
							StartCoroutine (GestionDesVagues ());
						}
					}
				}*/
            }
        }
    }

    public void InitSuiteVagueBoss()
    {
        scriptGlobal.nbrVagues++;
        StartCoroutine(GestionDesVagues());
    }

    //Gestion des vagues
    private void GestionVague()
    {
        MaJNumeroVague();
        Vague testVague = new Vague(scriptGlobal.nbrVagues);
        //On fait apparaitre les ennemis toutes les secondes
        StartCoroutine(AfficherEnnemi(testVague.tabTypeEnnemis, 0));
        scriptGlobal.nbrVagues++;
    }
    IEnumerator AfficherEnnemi(int[] tabTypes, int c)
    {
        //On choisit d'abord un point d'apparition
        int zoneApparition = (int)Random.Range(1f, 8.5f); //Entre 1 t 8
        GameObject zone = GameObject.Find("ZoneApparition" + zoneApparition).gameObject;
        //On affiche l'ennemi
        //Debug.Log ("Afficher Ennemi: "+scriptGlobal.bossTime);
        if (tabTypes[c] < 1000)
        {
            GameObject cultiste = Instantiate(Resources.Load("Cultiste" + tabTypes[c])) as GameObject;
            cultiste.transform.position = new Vector2(zone.transform.position.x, zone.transform.position.y);
            cultiste.transform.parent = GameObject.Find("LesCultistes").transform;
            //Fx
            scriptFX.Instance.MakeFx("FxCultiste1");
        }
        //Boss
        else {
            scriptGlobal.bossTime = true;
            GameObject boss = Instantiate(Resources.Load("CultBoss1")) as GameObject;
            boss.transform.position = new Vector2(zone.transform.position.x, zone.transform.position.y);
            boss.transform.parent = GameObject.Find("LesCultistes").transform;
			//Si boss de fin
			if(scriptGlobal.nbrVagues == (5*scriptGlobal.pallierVagues)) {
				boss.transform.GetComponent<SpriteRenderer>().color = Color.black;
				boss.transform.localScale = new Vector3(2f, 2f, 0);
			}
			//Musique boss
			scriptMusiques.Instance.MakeTheme("ThemeBoss1");
        }
        c++;
        //Suite
        if (c < tabTypes.Length)
        {
		yield return new WaitForSeconds(scriptGlobal.interval2ennemis);
            if (scriptGlobal.etatJeu == "play") StartCoroutine(AfficherEnnemi(tabTypes, c));
        }
        //Sinon fin
    }

    //FX
    //Penta qui clignote
    public void FxPentaBrille(float duree, int color)
    {
        switch (color)
        {
            case 0:
                fxPenta.GetComponent<SpriteRenderer>().color = Color.red;
                break;
            case 1:
                fxPenta.GetComponent<SpriteRenderer>().color = Color.blue;
                break;
            case 2:
                fxPenta.GetComponent<SpriteRenderer>().color = Color.green;
                break;
			case 3:
				fxPenta.GetComponent<SpriteRenderer>().color = Color.yellow;
				break;
			case 4:
				fxPenta.GetComponent<SpriteRenderer>().color = Color.black;
				break; 
        }
        fxPenta.SetActive(true);
        StartCoroutine(PentaBrille(duree));
    }
    IEnumerator PentaBrille(float duree)
    {
        yield return new WaitForSeconds(duree);
        fxPenta.SetActive(false);
    }

    //Effet discotheque
    private void FxDisco()
    {
		fxDicso1.SetActive(true);
		fxDicso2.SetActive(true);
    }
    private void TerminerFxDisco()
    {
        fxDicso1.SetActive(false);
        fxDicso2.SetActive(false);
    }

    //Ecrans de jeu
    public void ClicEcranTitre()
    {
        if (scriptGlobal.etatJeu == "init")
        {
            scriptGlobal.etatJeu = "intro";
            //Audio
            //scriptMusiques.Instance.FadeTheme();
            scriptFX.Instance.MakeFx("FxClic1");
            //On efface les textes
            ecranTitre.transform.FindChild("Image").transform.gameObject.SetActive(false);
			ecranTitre.transform.FindChild("BtnMode1P").gameObject.SetActive(false);
			ecranTitre.transform.FindChild("BtnMode2P").gameObject.SetActive(false);
			ecranTitre.transform.FindChild("Text4").GetComponent<Text>().gameObject.SetActive(false);
			ecranTitre.transform.FindChild("Consigne").GetComponent<Text>().gameObject.SetActive(false);
			//On affiche les bandes cinematiques
			GameObject.Find("Canvas").transform.FindChild("Cine1").gameObject.SetActive(true);
			GameObject.Find("Canvas").transform.FindChild("Cine2").gameObject.SetActive(true);
			//Info combi
			GameObject.Find("Canvas").transform.FindChild("InfoCombi").gameObject.SetActive(false);
            //On zoome sur la porte
            iTween.ScaleTo(ecranTitre, iTween.Hash("scale", new Vector3(8f, 8f, 0f), "time", 2f, "oncomplete", "", "easetype", "Linear", "oncompletetarget", gameObject));
            iTween.MoveTo(ecranTitre, iTween.Hash("position", new Vector3(ecranTitre.transform.position.x, ecranTitre.transform.position.y + Screen.height * 2, 0f), "time", 2f, "oncomplete", "FinZoom", "easetype", "Linear", "oncompletetarget", gameObject));
        }
    }
    private void FinZoom()
    {
        ecranTitre.SetActive(false);
        StartCoroutine(EffetEscalier());
    }
    IEnumerator EffetEscalier()
    {
        yield return new WaitForSeconds(0.5f);
        //On zoom sur la porte
        iTween.ScaleTo(ecranIntro, iTween.Hash("scale", new Vector3(4f, 4f, 0f), "time", 3f, "oncomplete", "", "easetype", "Linear", "oncompletetarget", gameObject));
		iTween.MoveTo(ecranIntro, iTween.Hash("position", new Vector3(ecranIntro.transform.position.x, ecranIntro.transform.position.y - Screen.height/3*4, 0f), "time", 3f, "oncomplete", "", "easetype", "Linear", "oncompletetarget", gameObject));
        yield return new WaitForSeconds(3.1f);
        ecranIntro.SetActive(false);
		//Musique
		//scriptMusiques.Instance.StopTheme();
		//scriptFX.Instance.MakeFx("FxIntro");
		message.GetComponent<Text>().text = "CHICKEN MUST DIE !!!";
        yield return new WaitForSeconds(3f);
        message.GetComponent<Text>().text = "";
        GameObject.Find("World").GetComponent<MiseEnSceneSacrifice>().lancerSacrifice();
    }

	//Gestion audio
	public void ClicBtnAudio()
	{
		if(scriptGlobal.volumeMusique > 0) {
			scriptGlobal.volumeMusique = 0f;
			GameObject.Find("Canvas").transform.FindChild("BtnAudio").transform.GetComponent<Image>().color = new Color(GameObject.Find("Canvas").transform.FindChild("BtnAudio").transform.GetComponent<Image>().color.r, GameObject.Find("Canvas").transform.FindChild("BtnAudio").transform.GetComponent<Image>().color.g, GameObject.Find("Canvas").transform.FindChild("BtnAudio").transform.GetComponent<Image>().color.b, 0.5f);
		}
		else {
			scriptGlobal.volumeMusique = 0.8f;
			GameObject.Find("Canvas").transform.FindChild("BtnAudio").transform.GetComponent<Image>().color = new Color(GameObject.Find("Canvas").transform.FindChild("BtnAudio").transform.GetComponent<Image>().color.r, GameObject.Find("Canvas").transform.FindChild("BtnAudio").transform.GetComponent<Image>().color.g, GameObject.Find("Canvas").transform.FindChild("BtnAudio").transform.GetComponent<Image>().color.b, 1f);
		}
	}
}
