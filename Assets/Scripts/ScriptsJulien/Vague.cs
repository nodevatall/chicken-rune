﻿using UnityEngine;
using System.Collections;

public class Vague : MonoBehaviour {

	private int _numeroVague;
	private int[] _tabEnnemis;

	//Constructeur
	public Vague(int numeroVague)
	{
		_numeroVague = numeroVague;
		//En fonction du numero de la vague, le nombre d'ennemis augmente (= numeroVague +2)
		ChoixTypesEnnemis();
		//TEST
		string resultat = "";
		for(int c = 0; c < _tabEnnemis.Length; c++)
		{
			resultat += _tabEnnemis [c] + " / ";
		}
		//print(resultat);
	}

	//Choix des types d'ennemis
	/*private void ChoixTypesEnnemis()
	{
		int nbrEnnemis = 1;
		//Cas des boss
		if (_numeroVague % 5 == 0)
		{
			_tabEnnemis = new int[1];
			_tabEnnemis[0] = 1000;
		}
		else
		{
			if(scriptGlobal.joueur2Mode == true) nbrEnnemis = _numeroVague/3+4; 
			else nbrEnnemis = _numeroVague/3+2;
			_tabEnnemis = new int[nbrEnnemis];
			for(int c = 0; c < nbrEnnemis; c++)
			{
				//Choix de l'ennemi
				int typeEnnemi = 1;
				if (_numeroVague < 6)
				{
					typeEnnemi = 1;
				} else if (_numeroVague < 11)
				{
					typeEnnemi = (int)Random.Range (1f, 2.5f); //Entre 1 t 2
				} else if (_numeroVague < 16)
				{
					typeEnnemi = (int)Random.Range (1f, 3.5f); //Entre 1 t 3
				} else if (_numeroVague < 21)
				{
					typeEnnemi = (int)Random.Range (2f, 4.5f); //Entre 2 t 4
				} else if (_numeroVague < 26)
				{
					typeEnnemi = (int)Random.Range (3f, 4.5f); //Entre 3 t 4
				}
				_tabEnnemis[c] = typeEnnemi;
			}
		}
	}*/

	private void ChoixTypesEnnemis()
	{
		int nbrEnnemis = 1;
		//Cas des boss
		if (_numeroVague % scriptGlobal.pallierVagues == 0)
		{
			_tabEnnemis = new int[1];
			_tabEnnemis[0] = 1000;
		}
		else
		{
			nbrEnnemis = _numeroVague/5+2;
			_tabEnnemis = new int[nbrEnnemis];
			for(int c = 0; c < nbrEnnemis; c++)
			{
				//Choix de l'ennemi
				int typeEnnemi = 1;
				if (_numeroVague < scriptGlobal.pallierVagues)
				{
					typeEnnemi = 1;
				} else if (_numeroVague < (2*scriptGlobal.pallierVagues)+1)
				{
					typeEnnemi = (int)Random.Range (1f, 2.5f); //Entre 1 t 2
				} else if (_numeroVague < (3*scriptGlobal.pallierVagues)+1)
				{
					typeEnnemi = (int)Random.Range (1f, 3.5f); //Entre 1 t 3
				} else if (_numeroVague < (4*scriptGlobal.pallierVagues)+1)
				{
					typeEnnemi = (int)Random.Range (2f, 4.5f); //Entre 2 t 4
				} else if (_numeroVague < (5*scriptGlobal.pallierVagues)+1)
				{
					typeEnnemi = (int)Random.Range (3f, 4.5f); //Entre 3 t 4
				}
				_tabEnnemis[c] = typeEnnemi;
			}
		}
	}

	public int[] tabTypeEnnemis
	{
		get { return this._tabEnnemis; }
	}
}
