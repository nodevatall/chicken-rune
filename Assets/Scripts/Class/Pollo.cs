﻿using UnityEngine;
using System.Collections;

public class Pollo : MonoBehaviour {

    private float _speed = 15f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Rigidbody2D rbd = GetComponent<Rigidbody2D>();
        float moveX = Input.GetAxis("Horizontal");
        float moveY = Input.GetAxis("Vertical");
        rbd.velocity = new Vector2(moveX * this._speed, moveY * this._speed);
    }
}
