﻿using UnityEngine;
using System.Collections;

public class Cultiste {
    private int _id;
    private int _type;
    private int _vitesse;
    private int _vie;
    private int _degats;

    //Constructeur
    public Cultiste(int unId, int unType)//Toutes les propriétés a 1 de base
    {
        this._id = unId;
        this._type = unType;
        this._vitesse = 1;
        this._vie = 1;
        this._degats = 1;
    }
    public Cultiste(int unId,int unType,int uneVitesse,int uneVie,int unDegat)//Permet de set toutes les propriétés
    {
        this._id = unId;
        this._type = unType;
        this._vitesse = 1;
        this._vie = 1;
        this._degats = 1;
    }

    //Assesseurs :
    public int id
    {
        get { return this._id; }
    }
    public int type
    {
        get { return this._type; }
        set { this._type = value; }
    }
    public int vitesse
    {
        get { return this._vitesse ;  }
        set { this._vitesse = value; }
    }
    public int vie
    {
        get { return this._vie; }
        set { this._vie = value; }
    }
    public int degats
    {
        get { return this._degats; }
        set { this._degats = value; }
    }

    public bool infligerDegats(int nbrDegatsAInfliger)//Return true si le cultiste meurt de l'attaque
    {
        if (this._vie - nbrDegatsAInfliger <= 0)
        {
            //Le cultiste est mort :
            return (true);
        }
        else
        {
            this._vie -= nbrDegatsAInfliger;
            return (false);
        }
    }
}
