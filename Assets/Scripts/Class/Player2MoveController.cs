﻿using UnityEngine;
using System.Collections;

public class Player2MoveController : MonoBehaviour {

    private float _speed = 3f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        this._speed = scriptGlobal.vitessePoulet;
        float moveX = Input.GetAxis("Horizontal2");//ChangerLesAxis pour 2ieme joueur
        float moveY = Input.GetAxis("Vertical2");//ChangerLesAxis pour 2ieme joueur
        GetComponent<Rigidbody2D>().velocity = new Vector2(moveX * this._speed, moveY * this._speed);
    }

    void SetSpeed(int speed)
    {
        this._speed = speed;
    }
}
