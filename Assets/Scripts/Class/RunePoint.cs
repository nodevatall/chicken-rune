﻿using UnityEngine;
using System.Collections;

public class RunePoint : MonoBehaviour
{

    private bool _active;
    private Animator _anim;

    // Use this for initialization
    void Start()
    {
        this._active = false;
        this._anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    // On collision
    void OnTriggerEnter2D(Collider2D other)
    {
        if (!this._active)
        {
            if (other.CompareTag("Player"))
            {
                this._active = true;
                this._anim.SetBool("Active",true);
            }
        }
    }
}
