﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Sort {
    private List<int> _combinaison;
    private List<int> _ennemiCible;

    public Sort(List<int> mesCombinaison,List<int> mesEnnemiCible)
    {
        this._combinaison = mesCombinaison;
        this._ennemiCible = mesEnnemiCible;
    }
    public List<int> combinaison
    {
        get { return this._combinaison; }
    }
    public List<int> ennemiCible
    {
        get { return this._ennemiCible; }
    }
}
