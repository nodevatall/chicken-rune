﻿using UnityEngine;
using System.Collections;

public class RuneScript : MonoBehaviour {
    public int idRune;
    public bool activer=false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if ((other.gameObject.name.Equals("Joueur")|| other.gameObject.name.Equals("Joueur2"))&&scriptGlobal.sortEnCour==false)
        {
            if (activer == false)
            {
                //print("Joueur sur la rune : " + idRune.ToString());
                //Fx rune
				scriptFX.Instance.MakeFx("FxRune" + (idRune+1));
				//activer la rune :
                activer = true;
				GetComponent<Animator> ().SetBool ("active",true);
               // this.GetComponent<SpriteRenderer>().color = new Color(this.GetComponent<SpriteRenderer>().color.r, this.GetComponent<SpriteRenderer>().color.g, this.GetComponent<SpriteRenderer>().color.g, 0.35f);
                GameObject.Find("World").GetComponent<RunesManager>().addRune(idRune);
                //lancer state anim rune activer :
            }
            else
            {
                desactiverRune();
            }
        }

    }
    public void desactiverRune()//Procédure lancer par le Script Manager
    {
		//Fx rune
		scriptFX.Instance.MakeFx("FxSwitch1");
		//Desactiver la rune :
        activer = false;
        GameObject.Find("World").GetComponent<RunesManager>().runeActiver.Remove(idRune);
        //print("Nbr de Rune Activer :" + GameObject.Find("World").GetComponent<RunesManager>().runeActiver.Count.ToString());
       // this.GetComponent<SpriteRenderer>().color = new Color(this.GetComponent<SpriteRenderer>().color.r, this.GetComponent<SpriteRenderer>().color.g, this.GetComponent<SpriteRenderer>().color.g, 1f);
        //lancer state anim rune desactiver :
		GetComponent<Animator> ().SetBool ("active",false);
    }
}
