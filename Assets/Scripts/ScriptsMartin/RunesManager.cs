﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RunesManager : MonoBehaviour {
    private List<Sort> listSorts = new List<Sort>();
    public List<int> runeActiver = new List<int>();
    private List<int> listTempEnnemiCibe;
    public GameObject Pentagramme;
    void Awake()
    {
        //Sort 1
        List<int> combinaisonSort1 = new List<int>();
        combinaisonSort1.Add(0);
        combinaisonSort1.Add(4);
        List<int> ennemiCibleSort1 = new List<int>();
        ennemiCibleSort1.Add(0);
        listSorts.Add(new Sort(combinaisonSort1, ennemiCibleSort1));
        //Sort 2
        List<int> combinaisonSort2 = new List<int>();
        combinaisonSort2.Add(1);
        combinaisonSort2.Add(3);
        List<int> ennemiCibleSort2 = new List<int>();
        ennemiCibleSort2.Add(1);
        listSorts.Add(new Sort(combinaisonSort2, ennemiCibleSort2));
        //Sort 3
        List<int> combinaisonSort3 = new List<int>();
        combinaisonSort3.Add(3);
        combinaisonSort3.Add(2);
        combinaisonSort3.Add(4);
        List<int> ennemiCibleSort3 = new List<int>();
        ennemiCibleSort3.Add(2);
        listSorts.Add(new Sort(combinaisonSort3, ennemiCibleSort3));
		//Sort 4
		List<int> combinaisonSort4 = new List<int>();
		combinaisonSort4.Add(4);
		combinaisonSort4.Add(2);
		combinaisonSort4.Add(0);
		List<int> ennemiCibleSort4 = new List<int>();
		ennemiCibleSort4.Add(3);
		listSorts.Add(new Sort(combinaisonSort4, ennemiCibleSort4));
        //Sort 6 BOSS
        List<int> combinaisonSort6 = new List<int>();
        combinaisonSort6.Add(0);
        combinaisonSort6.Add(1);
        combinaisonSort6.Add(3);
        List<int> ennemiCibleSort6 = new List<int>();
        ennemiCibleSort6.Add(6);
        listSorts.Add(new Sort(combinaisonSort6, ennemiCibleSort6));
    }
    public void addRune(int maRune)
    {
        runeActiver.Add(maRune);
        testerSort();
    }
    private void testerSort()
    {
        foreach(Sort unSort in listSorts)
        {
            if (unSort.combinaison.Count == runeActiver.Count)
            {
                //print("Rentrer 1 testerSort");
                bool correspondance = true;
                for(int i = 0; i < runeActiver.Count; i++)
                {
                    if (runeActiver[i] != unSort.combinaison[i])
                    {
                        //print("Pas la bonne combinaison");
                        correspondance = false;
                    }
                }
                if (correspondance == true)
                {
                    scriptGlobal.sortEnCour = true;
                    sortReussi(unSort.ennemiCible);
                    //lancer animation du sort :
					//print("listSorts.IndexOf(unSort) : " + listSorts.IndexOf(unSort));
                    GameObject.Find("Scripts").GetComponent<Main>().FxPentaBrille(3, listSorts.IndexOf(unSort));
                }
            }
        }
    }
    private void sortReussi(List<int> typeEnnemiCible)
    {
        //Sort Finie :
        listTempEnnemiCibe = typeEnnemiCible;
        Invoke("sortFini",3);//Ou lancer par l'anim a voir si possible

    }
    public void sortFini()
    {
        scriptGlobal.sortEnCour = false;
		if(scriptGlobal.etatJeu == "play") {
	        //Supprimmer les ennemi dans le trigger du pentagramme qui correspond aux type :
	        Pentagramme.GetComponent<pentagrammeScript>().killEnnemi(listTempEnnemiCibe);
	        //Desactiver runes :
	        foreach(Transform uneRune in Pentagramme.transform)
	        {
	            uneRune.GetComponent<RuneScript>().desactiverRune();
	        }
		}
    }

	public void resetRunes()
	{
		foreach(Transform uneRune in Pentagramme.transform)
		{
				uneRune.GetComponent<RuneScript>().desactiverRune();
		}
	}
}
