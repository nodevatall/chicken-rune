﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {
    public int vitesse = 10;
    public Vector3 positionCible;
    private bool JoueurToucher = false;
    private string nomJoueur;
    // Use this for initialization
    void Start () {
        positionCible = GameObject.Find("Joueur").transform.position;
        if (scriptGlobal.joueur2Mode)
        {
            GameObject Cible = GameObject.Find("Joueur");
            GameObject Cible2 = GameObject.Find("Joueur2");
            Vector3[] tabPosition = new Vector3[2] { (Cible.transform.position), (Cible2.transform.position) };
            if ((tabPosition[0].x - this.transform.position.x) + (tabPosition[0].y - this.transform.position.y) < (tabPosition[1].x - this.transform.position.x) + (tabPosition[1].y - this.transform.position.y))
            {
                positionCible = Cible.transform.position;
            }
            else
            {
                positionCible = Cible2.transform.position;
            }
        }
        Invoke("supprimerProjectile", 1.5f);
    }
    void FixedUpdate()
    {
        transform.position = Vector2.Lerp(this.GetComponent<Transform>().position, positionCible, (0.01f * vitesse));
    }
    void OnTriggerEnter2D(Collider2D other)
    {
		if((other.gameObject.name.Equals("Joueur")|| other.gameObject.name.Equals("Joueur2")) && JoueurToucher == false && scriptGlobal.pouletInvincible == false)
        {
            nomJoueur = other.gameObject.name;
            JoueurToucher = true;
            //Perte de vie du poulet
            GameObject.Find("Scripts").GetComponent<Main>().ModifierVie(-1);
			other.GetComponent<Animator>().SetTrigger("hurt");
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {

        if (other.gameObject.name.Equals(nomJoueur) && JoueurToucher == true)
        {
            JoueurToucher = false;
        }
    }
    public void supprimerProjectile()
    {
        Destroy(this.gameObject);
    }

}
