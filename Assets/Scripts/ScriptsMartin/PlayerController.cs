﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour
{
    public float tilt;
    public Boundary boundary;

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector3 movement = new Vector2(moveHorizontal, moveVertical);
		this.GetComponent<Rigidbody2D>().velocity = movement * scriptGlobal.vitessePoulet;
        this.GetComponent<Rigidbody2D>().position = new Vector2(Mathf.Clamp(this.GetComponent<Rigidbody2D>().position.x, boundary.xMin, boundary.xMax),Mathf.Clamp(this.GetComponent<Rigidbody2D>().position.y, boundary.zMin, boundary.zMax));
    }
}
